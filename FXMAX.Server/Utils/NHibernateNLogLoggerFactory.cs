﻿using System;
using NHibernate;

namespace FXMAX.Server.Utils
{
    public class NHibernateNLogLoggerFactory : ILoggerFactory
    {
        public IInternalLogger LoggerFor(string keyName)
        {
            return new NHibernateNLogLogger(keyName);
        }

        public IInternalLogger LoggerFor(Type type)
        {
            return new NHibernateNLogLogger(type.Name);
        }
    }
}