using System;
using NHibernate;
using NLog;

namespace FXMAX.Server.Utils
{
    public class NHibernateNLogLogger : IInternalLogger
    {
        private readonly Logger logger;

        public NHibernateNLogLogger(string name)
        {
            logger = LogManager.GetLogger(name);
        }

        public void Error(object message)
        {
            logger.Error(message.ToString());
        }

        public void Error(object message, Exception exception)
        {
            logger.ErrorException(message.ToString(), exception);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            logger.Error(format, args);
        }

        public void Fatal(object message)
        {
            logger.Fatal(message.ToString());
        }

        public void Fatal(object message, Exception exception)
        {
            logger.FatalException(message.ToString(), exception);
        }

        public void Debug(object message)
        {
            logger.Debug(message.ToString());
        }

        public void Debug(object message, Exception exception)
        {
            logger.DebugException(message.ToString(), exception);
        }

        public void DebugFormat(string format, params object[] args)
        {
            logger.Debug(format, args);
        }

        public void Info(object message)
        {
            logger.Info(message.ToString);
        }

        public void Info(object message, Exception exception)
        {
            logger.InfoException(message.ToString(), exception);
        }

        public void InfoFormat(string format, params object[] args)
        {
            logger.Info(format, args);
        }

        public void Warn(object message)
        {
            logger.Warn(message.ToString());
        }

        public void Warn(object message, Exception exception)
        {
            logger.WarnException(message.ToString(), exception);
        }

        public void WarnFormat(string format, params object[] args)
        {
            logger.Warn(format, args);
        }

        public bool IsErrorEnabled
        {
            get { return logger.IsErrorEnabled; }
        }

        public bool IsFatalEnabled
        {
            get { return logger.IsFatalEnabled; }
        }

        public bool IsDebugEnabled
        {
            get { return logger.IsDebugEnabled; }
        }

        public bool IsInfoEnabled
        {
            get { return logger.IsInfoEnabled; }
        }

        public bool IsWarnEnabled
        {
            get { return logger.IsWarnEnabled; }
        }
    }
}