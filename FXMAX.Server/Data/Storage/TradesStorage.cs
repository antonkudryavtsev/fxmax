﻿using System;
using System.Collections.Generic;
using FXMAX.Server.Data.Entities;
using NHibernate;

namespace FXMAX.Server.Data.Storage
{
    public class TradesStorage : ITradesStorage
    {
        private readonly Func<ISession> getSession;

        public TradesStorage(Func<ISession> getSession)
        {
            this.getSession = getSession;
        }

        public IList<TradeEntity> GetAllTrades()
        {
            using(ISession session = getSession())
            {
                return session.QueryOver<TradeEntity>().List();
            }
        }

        public void CreateTrade(TradeEntity trade)
        {
            throw new System.NotImplementedException();
        }
    }
}