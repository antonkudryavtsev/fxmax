﻿using System.Collections.Generic;
using FXMAX.Server.Data.Entities;

namespace FXMAX.Server.Data.Storage
{
    public interface IPairsStorage
    {
        IList<PairEntity> GetAllPairs();

        void CreatePair(PairEntity pair);
    }
}