﻿using System.Collections.Generic;
using FXMAX.Server.Data.Entities;

namespace FXMAX.Server.Data.Storage
{
    public interface IClientsStorage
    {
        IList<ClientEntity> GetAllClients();

        void CreateClient(ClientEntity client);
    }
}