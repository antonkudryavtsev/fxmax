﻿using System.Collections.Generic;
using FXMAX.Server.Data.Entities;

namespace FXMAX.Server.Data.Storage
{
    public interface IBranchesStorage
    {
        IList<BranchEntity> GetAllBranches();

        void CreateBranch(BranchEntity branch);
    }
}