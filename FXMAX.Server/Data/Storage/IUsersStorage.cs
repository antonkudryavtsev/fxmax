﻿using System.Collections;
using System.Collections.Generic;
using FXMAX.Server.Data.Entities;

namespace FXMAX.Server.Data.Storage
{
    public interface IUsersStorage
    {
        /// <summary>
        /// Tries to get user with given <see cref="login"/> and <see cref="password"/>
        /// </summary>
        /// <returns>Matched user or null if user doesn't exist</returns>
        UserEntity AuthenticateUser(string login, string password);

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns>List of users</returns>
        IList<UserEntity> GetUsersList();
    }
}