﻿using System.Collections.Generic;
using FXMAX.Server.Data.Entities;

namespace FXMAX.Server.Data.Storage
{
    public interface ITradesStorage
    {
        IList<TradeEntity> GetAllTrades();

        void CreateTrade(TradeEntity trade);
    }
}