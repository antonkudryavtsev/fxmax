﻿using System;
using System.Collections.Generic;
using FXMAX.Server.Data.Entities;
using NHibernate;

namespace FXMAX.Server.Data.Storage
{
    public class CurrenciesStorage : ICurrenciesStorage
    {
        private readonly Func<ISession> getSession;

        public CurrenciesStorage(Func<ISession> getSession)
        {
            this.getSession = getSession;
        }

        public IList<CurrencyEntity> GetAllCurrencies()
        {
            using (ISession session = getSession())
            {
                return session.QueryOver<CurrencyEntity>().List();
            }
        }

        public void CreateCurrency(CurrencyEntity currency)
        {
            throw new System.NotImplementedException();
        }
    }
}