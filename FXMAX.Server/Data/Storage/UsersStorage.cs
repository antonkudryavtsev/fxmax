﻿using System;
using System.Collections.Generic;
using FXMAX.Server.Data.Entities;
using NHibernate;

namespace FXMAX.Server.Data.Storage
{
    public class UsersStorage : IUsersStorage
    {
        private readonly Func<ISession> getSession;

        public UsersStorage(Func<ISession> getSession)
        {
            this.getSession = getSession;
        }

        public UserEntity AuthenticateUser(string login, string password)
        {
            using (ISession session = getSession())
            {
                return session.QueryOver<UserEntity>().Where(u => u.Login == login && u.Password == password).SingleOrDefault();
            }
        }

        public IList<UserEntity> GetUsersList()
        {
            using (ISession session = getSession())
            {
                return session.QueryOver<UserEntity>().List();
            }
        }
    }
}
