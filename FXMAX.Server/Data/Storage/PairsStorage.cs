﻿using System;
using System.Collections.Generic;
using FXMAX.Server.Data.Entities;
using NHibernate;

namespace FXMAX.Server.Data.Storage
{
    public class PairsStorage : IPairsStorage
    {
        private readonly Func<ISession> getSession;

        public PairsStorage(Func<ISession> getSession)
        {
            this.getSession = getSession;
        }

        public IList<PairEntity> GetAllPairs()
        {
            using (ISession session = getSession())
            {
                return session.QueryOver<PairEntity>().List();
            }
        }

        public void CreatePair(PairEntity pair)
        {
            throw new System.NotImplementedException();
        }
    }
}