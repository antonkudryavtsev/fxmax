﻿using System;
using System.Collections.Generic;
using FXMAX.Server.Data.Entities;
using NHibernate;

namespace FXMAX.Server.Data.Storage
{
    public class ClientsStorage : IClientsStorage
    {
        private readonly Func<ISession> getSession;

        public ClientsStorage(Func<ISession> getSession)
        {
            this.getSession = getSession;
        }

        public IList<ClientEntity> GetAllClients()
        {
            using (ISession session = getSession())
            {
                return session.QueryOver<ClientEntity>().List();
            }
        }

        public void CreateClient(ClientEntity client)
        {
            using (ISession session = getSession())
            {
                session.Save(client);
            }
        }
    }
}