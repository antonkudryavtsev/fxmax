using System;
using System.Collections.Generic;
using FXMAX.Server.Data.Entities;
using NHibernate;

namespace FXMAX.Server.Data.Storage
{
    public class BranchesStorage : IBranchesStorage
    {
        private readonly Func<ISession> getSession;

        public BranchesStorage(Func<ISession> getSession)
        {
            this.getSession = getSession;
        }

        public IList<BranchEntity> GetAllBranches()
        {
            using (ISession session = getSession())
            {
                return session.QueryOver<BranchEntity>().List();
            }
        }

        public void CreateBranch(BranchEntity branch)
        {
            throw new System.NotImplementedException();
        }
    }
}