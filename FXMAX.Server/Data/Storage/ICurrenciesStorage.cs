﻿using System.Collections.Generic;
using FXMAX.Server.Data.Entities;

namespace FXMAX.Server.Data.Storage
{
    public interface ICurrenciesStorage
    {
        IList<CurrencyEntity> GetAllCurrencies();

        void CreateCurrency(CurrencyEntity currency);
    }
}