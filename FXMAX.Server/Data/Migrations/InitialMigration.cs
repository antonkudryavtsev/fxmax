﻿using FluentMigrator;

namespace FXMAX.Server.Data.Migrations
{
    [Migration(1)]
    public class InitialMigration : Migration
    {
        private const string UsersTableName = "Users";
        private const string CurrenciesTableName = "Currencies";
        private const string PairsTableName = "Pairs";
        private const string ClientsTableName = "Clients";
        private const string BranchesTableName = "Branches";
        
        private const string TradesTable = "Trades";

        public override void Up()
        {
            // users
            CreateUsersTable(UsersTableName);

            // directories
            CreateCurrenciesTable(CurrenciesTableName);
            CreatePairsTable(PairsTableName);
            CreateClientTable(ClientsTableName);
            CreateBranchesTable(BranchesTableName);

            CreateTradesTable(TradesTable);
        }

        public override void Down()
        {
            // delete foreign keys
            Delete.ForeignKey("FK_TradeClient").OnTable(TradesTable);
            Delete.ForeignKey("FK_TradeBranch").OnTable(TradesTable);
            Delete.ForeignKey("FK_TradePair").OnTable(TradesTable);

            Delete.ForeignKey("FK_PairBaseCurrency").OnTable(PairsTableName);
            Delete.ForeignKey("FK_PairReferenceCurrency").OnTable(PairsTableName);

            Delete.Table(ClientsTableName);
            Delete.Table(BranchesTableName);
            Delete.Table(PairsTableName);
            Delete.Table(CurrenciesTableName);

            Delete.Table(UsersTableName);
        }

        private void CreateUsersTable(string usersTableName)
        {
            Create.Table(usersTableName)
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Login").AsString().NotNullable().Unique("IX_UserLogin")
                .WithColumn("Password").AsString().NotNullable();

            Execute.Sql("INSERT INTO [" + usersTableName + "] (Login, Password) VALUES ('admin', 'admin');");
        }

        private void CreateClientTable(string tableName)
        {
            Create.Table(tableName)
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Name").AsString().NotNullable();

            Execute.Sql("Set IDENTITY_INSERT [" + ClientsTableName + "] ON;");
            Execute.Sql("INSERT INTO [" + ClientsTableName + "] (Id, Name) VALUES (-1, 'Dummy Client')");
            Execute.Sql("Set IDENTITY_INSERT [" + ClientsTableName + "] OFF;");
        }

        private void CreateBranchesTable(string tableName)
        {
            Create.Table(tableName)
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Name").AsString().NotNullable();

            // main branch
            Execute.Sql("Set IDENTITY_INSERT [" + BranchesTableName + "] ON;");
            Execute.Sql("INSERT INTO [" + BranchesTableName + "] (Id, Name) VALUES (-1, 'Headquarters')");
            Execute.Sql("Set IDENTITY_INSERT [" + BranchesTableName + "] OFF;");
        }

        private void CreateCurrenciesTable(string tableName)
        {
            Create.Table(tableName)
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Name").AsString().NotNullable().Unique("IX_CurrencyName")
                .WithColumn("Code").AsString().NotNullable().Unique("IX_CurrencyCode")
                .WithColumn("DecimalPlaces").AsInt32().NotNullable().WithDefaultValue(2);

            // few default currencies
            Execute.Sql("Set IDENTITY_INSERT [" + CurrenciesTableName + "] ON;");
            Execute.Sql("INSERT INTO [" + CurrenciesTableName + "] (Id, Name, Code) "
                        + "VALUES (1, 'Russia Ruble', 'RUB'), (2, 'United States Dollar', 'USD'), (3, 'Euro', 'EUR');");
            Execute.Sql("Set IDENTITY_INSERT [" + CurrenciesTableName + "] OFF;");
        }

        private void CreatePairsTable(string tableName)
        {
            Create.Table(tableName)
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Code").AsString().NotNullable().Unique("IX_PairName")
                .WithColumn("BaseCurrencyId").AsInt64().ForeignKey("FK_PairBaseCurrency", CurrenciesTableName, "Id")
                .WithColumn("ReferenceCurrencyId").AsInt64().ForeignKey("FK_PairReferenceCurrency", CurrenciesTableName, "Id")
                .WithColumn("DecimalPlaces").AsInt32().NotNullable().WithDefaultValue(4);

            // few default pairs
            Execute.Sql("Set IDENTITY_INSERT [" + PairsTableName + "] ON;");
            Execute.Sql("INSERT INTO [" + PairsTableName + "] (Id, Code, BaseCurrencyId, ReferenceCurrencyId, DecimalPlaces) "
                        + "VALUES(1, 'USDRUB', 2, 1, 4), (2, 'EURRUB', 3, 1, 4), (3, 'ERUUSD', 3, 2, 4)");
            Execute.Sql("Set IDENTITY_INSERT [" + PairsTableName + "] OFF;");
        }

        private void CreateTradesTable(string tableName)
        {
            Create.Table(tableName)
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("ClientId").AsInt64().NotNullable().ForeignKey("FK_TradeClient", ClientsTableName, "Id")
                .WithColumn("BranchId").AsInt64().NotNullable().ForeignKey("FK_TradeBranch", BranchesTableName, "Id")
                .WithColumn("PairId").AsInt64().NotNullable().ForeignKey("FK_TradePair", PairsTableName, "Id")
                .WithColumn("BaseAmount").AsDecimal().NotNullable()
                .WithColumn("ReferenceAmount").AsDecimal().NotNullable()
                .WithColumn("Rate").AsDecimal().NotNullable()
                .WithColumn("Side").AsInt16().NotNullable()
                .WithColumn("Term").AsInt16().NotNullable();
        }
    }
}