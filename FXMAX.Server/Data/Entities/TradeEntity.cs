﻿using FXMAX.Core;
using FXMAX.Core.Enum;
using FXMAX.Server.Data.Converters;
using NHibernate.Mapping.Attributes;

namespace FXMAX.Server.Data.Entities
{
    [Class(Table = "Trades", NameType = typeof(TradeEntity))]
    public class TradeEntity
    {
        [Id(Column = "Id", TypeType = typeof(long))]
        [Generator(1, Class = "native")]
        public virtual long Id { get; protected set; }

        [Property(Column = "BaseAmount", NotNull = true)]
        public virtual decimal BaseAmount { get; set; }

        [Property(Column = "ReferenceAmount", NotNull = true)]
        public virtual decimal ReferenceAmount { get; set; }

        [Property(Column = "Rate", NotNull = true)]
        public virtual decimal Rate { get; set; }

        [Property(Column = "Side", NotNull = true, TypeType = typeof(SideStringType))]
        public virtual Side Side { get; set; }

        [Property(Column = "Term", NotNull = true, TypeType = typeof(TermStringType))]
        public virtual Term Term { get; set; }

        [ManyToOne(Column = "ClientId", NotNull = true)]
        public virtual ClientEntity Client { get; set; }

        [ManyToOne(Column = "BranchId", NotNull = true)]
        public virtual BranchEntity Branch { get; set; }

        [ManyToOne(Column = "PairId", NotNull = true)]
        public virtual PairEntity Pair { get; set; }
    }
}