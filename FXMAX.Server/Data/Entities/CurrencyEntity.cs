﻿using System.Collections.Generic;
using NHibernate.Mapping.Attributes;

namespace FXMAX.Server.Data.Entities
{
    [Class(Table = "Currencies", NameType = typeof(CurrencyEntity))]
    public class CurrencyEntity
    {
        [Id(Column = "Id", TypeType = typeof(long))]
        [Generator(1, Class = "native")]
        public virtual long Id { get; protected set; }

        [Property(Column = "Name", NotNull = true)]
        public virtual string Name { get; set; }

        [Property(Column = "Code", NotNull = true)]
        public virtual string Code { get; set; }

        [Property(Column = "DecimalPlaces", NotNull = true)]
        public virtual int DecimalPlaces { get; set; }

        [OneToMany]
        public virtual IList<PairEntity> Pairs { get; set; }
    }
}