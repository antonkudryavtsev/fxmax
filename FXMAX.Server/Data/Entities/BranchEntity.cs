﻿using System.Collections.Generic;
using NHibernate.Mapping.Attributes;

namespace FXMAX.Server.Data.Entities
{
    [Class(Table = "Branches", NameType = typeof(BranchEntity))]
    public class BranchEntity
    {
        [Id(Column = "Id", TypeType = typeof(long))]
        [Generator(1, Class = "native")]
        public virtual long Id { get; protected set; }

        [Property(Column = "Name", NotNull = true)]
        public virtual string Name { get; set; }

        [OneToMany]
        public virtual IList<TradeEntity> Trades { get; set; }
    }
}