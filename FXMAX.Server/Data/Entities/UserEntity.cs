﻿using FXMAX.Core;
using FXMAX.Core.Enum;
using FXMAX.Server.Data.Converters;
using NHibernate.Mapping.Attributes;

namespace FXMAX.Server.Data.Entities
{
    [Class(Table = "Users")]
    public class UserEntity
    {
        [Id(Column = "Id", TypeType = typeof(long))]
        [Generator(1, Class = "native")]
        public virtual long Id { get; set; }

        [Property(Column = "Login", NotNull = true)]
        public virtual string Login { get; set; }

        [Property(Column = "Password", NotNull = true)]
        public virtual string Password { get; set; }
    }
}