﻿using System.Collections.Generic;
using NHibernate.Mapping.Attributes;

namespace FXMAX.Server.Data.Entities
{
    [Class(Table = "Pairs", NameType = typeof(PairEntity))]
    public class PairEntity
    {
        [Id(Column = "Id", TypeType = typeof(long))]
        [Generator(1, Class = "native")]
        public virtual long Id { get; protected set; }

        [Property(Column = "Code")]
        public virtual string Code { get; set; }

        [Property(Column = "DecimalPlace")]
        public virtual int DecimalPlaces { get; set; }

        [ManyToOne(Column = "BaseCurrencyId", NotNull = true, ClassType = typeof(CurrencyEntity))]
        public virtual CurrencyEntity BaseCurrency { get; set; }

        [ManyToOne(Column = "ReferenceCurrencyId", NotNull = true, ClassType = typeof(CurrencyEntity))]
        public virtual CurrencyEntity ReferenceCurrency { get; set; }

        [OneToMany]
        public virtual IList<TradeEntity> Trades { get; set; }
    }
}