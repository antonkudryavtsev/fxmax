﻿using FXMAX.Core;
using FXMAX.Core.Enum;
using NHibernate.Type;

namespace FXMAX.Server.Data.Converters
{
    public class TermStringType : EnumStringType<Term> { }
}