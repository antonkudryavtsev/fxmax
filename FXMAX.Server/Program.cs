﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Description;
using Castle.Facilities.Startable;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using FXMAX.Core.Contracts;
using FXMAX.Server.Data.Entities;
using FXMAX.Server.Data.Storage;
using FXMAX.Server.Service;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.Attributes;
using NLog;
using Environment = NHibernate.Cfg.Environment;

namespace FXMAX.Server
{
    class Program
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        static void Main()
        {
            logger.Info("Starting up server...");

            // configure nhibernate
            HbmSerializer serializer = HbmSerializer.Default;
            serializer.Validate = true;

            Configuration configuration = new Configuration();

            configuration.SetProperties(new Dictionary<string, string>
                    {
                        {"nhibernate-logger", "FXMAX.Server.Utils.NHibernateNLogLoggerFactory, FXMAX.Server.Utils"},
                        {Environment.ConnectionString, AppSettings.Default.DbConnection},
                        {Environment.ConnectionDriver, typeof(Sql2008ClientDriver).FullName},
                        {Environment.Dialect, typeof(MsSql2012Dialect).FullName},
                        {"show_sql", "true"},
                        {"format_sql", "true"},
                    });

            configuration.AddInputStream(serializer.Serialize(Assembly.GetExecutingAssembly()));

            ISessionFactory sessionFactory = configuration.BuildSessionFactory();

            // Initialize container and register components
            WindsorContainer container = new WindsorContainer();

            // add facilities
            container.AddFacility<StartableFacility>();

            container.Register(
                Component.For<Func<ISession>>().Instance(sessionFactory.OpenSession),

                Component.For<IClientsStorage>().ImplementedBy<ClientsStorage>(),
                Component.For<IBranchesStorage>().ImplementedBy<BranchesStorage>(),
                Component.For<ICurrenciesStorage>().ImplementedBy<CurrenciesStorage>(),
                Component.For<IPairsStorage>().ImplementedBy<PairsStorage>(),
                Component.For<ITradesStorage>().ImplementedBy<TradesStorage>(),
                Component.For<IUsersStorage>().ImplementedBy<UsersStorage>()
            );

            IClientsStorage clientsStorage = container.Resolve<IClientsStorage>();
            IList<ClientEntity> clients = clientsStorage.GetAllClients();

            ServiceHost serviceHost;
            string strAdr = "net.tcp://localhost:1234/FXMAX/";
            try
            {
                Uri adrbase = new Uri(strAdr);
                serviceHost = new ServiceHost(typeof(ClientsService), adrbase);
                serviceHost.Authentication.ServiceAuthenticationManager = new ServiceAuthenticationManager();
                NetTcpBinding tcpb = new NetTcpBinding();

                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                serviceHost.Description.Behaviors.Add(smb);
                serviceHost.AddServiceEndpoint(typeof(IMetadataExchange),
                  MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

                serviceHost.AddServiceEndpoint(typeof(IClientsService), tcpb, strAdr);
                serviceHost.Open();
                logger.Info("Service is running and listening at " + strAdr);
            }
            catch (Exception ex)
            {
                serviceHost = null;
                logger.Warn("Service can not be started as >> [" +
                  strAdr + "] \n\nError Message [" + ex.Message + "]");
            }

            Console.ReadLine();

            logger.Info("Shutting down server...");

            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            container.Dispose();
        }
    }
}
