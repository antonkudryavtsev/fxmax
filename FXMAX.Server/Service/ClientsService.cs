﻿using System.Collections.Generic;
using FXMAX.Core.Contracts;
using FXMAX.Core.Domain;

namespace FXMAX.Server.Service
{
    public class ClientsService : IClientsService
    {
        public IList<Client> GetClientsList()
        {
            return new List<Client>
            {
                new Client {Name = "First Clients"},
                new Client {Name = "Second Clients"}
            };
        }
    }
}