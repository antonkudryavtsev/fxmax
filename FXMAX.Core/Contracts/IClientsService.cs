﻿using System.Collections.Generic;
using System.ServiceModel;
using FXMAX.Core.Domain;

namespace FXMAX.Core.Contracts
{
    [ServiceContract]
    public interface IClientsService
    {
        [OperationContract]
        IList<Client> GetClientsList();
    }
}