﻿namespace FXMAX.Core.Enum
{
    public enum UserRole
    {
        Administrator,
        Manager,
        MasterManager,
        Operator,
        ReportViewer
    }
}
