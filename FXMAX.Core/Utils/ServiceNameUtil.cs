﻿using System;

namespace FXMAX.Core.Utils
{
    public static class ServiceNameUtil
    {
        /// <summary>
        /// Get service name by contract interface. 
        /// Converts interface name to service name by removing `I` in the beginning
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public static string GetServiceNameByContractType(Type serviceType)
        {
            string name = serviceType.Name;
            if (name.StartsWith("I"))
            {
                return name.Substring(1);
            }

            return name;
        }
    }
}