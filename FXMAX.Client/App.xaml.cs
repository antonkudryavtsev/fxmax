﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Windows;
using FXMAX.Core.Contracts;
using FXMAX.Core.Utils;

namespace FXMAX.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            NetTcpBinding binding = new NetTcpBinding();
            EndpointAddress address = new EndpointAddress(new Uri("net.tcp://localhost:1234/FXMAX"));
            ChannelFactory<IClientsService> factory = new ChannelFactory<IClientsService>(binding, address);

            IClientsService service = factory.CreateChannel();
            IList<Core.Domain.Client> clients = service.GetClientsList();

            int i = 1;
        }
    }

    public static class ServiceResolver
    {
    }
}
